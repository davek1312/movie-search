<?php

use Illuminate\Database\Seeder;
use App\User;

/**
 * Adds a test user to the users table
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $user = User::firstOrNew([
            'email' => 'test@test.com',
        ]);
        $user->fill([
            'name' => 'inmtask',
            'password' => bcrypt('test'),
        ]);
        $user->save();
    }
}
