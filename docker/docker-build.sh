#!/usr/bin/env bash

cd ..
cd movie-search-laradock

echo "Building docker containers"
docker-compose up -d nginx mysql
test "$?" -ne "0" && { echo "Could not build docker containers. Exiting." ; exit 1;}