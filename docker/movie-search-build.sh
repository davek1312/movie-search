#!/usr/bin/env bash

COMMAND_WORKSPACE="docker-compose exec --user=laradock workspace"

cd ..

echo "Creating .env file"
if [ ! -f .env ]; then
    cp .env.example .env
    cd movie-search-laradock
    $COMMAND_WORKSPACE php artisan key:generate
fi
test "$?" -ne "0" && { echo "Could not create .env file." ; exit 1;}