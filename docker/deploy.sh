#!/usr/bin/env bash

COMMAND_WORKSPACE="docker-compose exec --user=laradock workspace"

cd ..
cd movie-search-laradock

echo "Git pull"
git pull origin master
test "$?" -ne "0" && { echo "Could not git pull. Exiting." ; exit 1;}


echo "Composer install"
$COMMAND_WORKSPACE composer install
test "$?" -ne "0" && { echo "Could not composer install. Exiting." ; exit 1;}


echo "Database migration"
$COMMAND_WORKSPACE php artisan migrate
test "$?" -ne "0" && { echo "Could not migrate database. Exiting." ; exit 1;}

echo "Database seed"
$COMMAND_WORKSPACE php artisan db:seed
test "$?" -ne "0" && { echo "Could not seed database. Exiting." ; exit 1;}