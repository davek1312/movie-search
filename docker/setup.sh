#!/usr/bin/env bash

cd ..

echo "Adding Laradock"
git submodule add --force ssh://git@bitbucket.org/davek1312/movie-search-laradock.git
test "$?" -ne "0" && { echo "Could not add Larafock. Exiting." ; exit 1;}

echo "Creating Laradock .env file"
cd movie-search-laradock
if [ ! -f .env ]; then
    cp env-example .env
fi
test "$?" -ne "0" && { echo "Could not create Laradock .env file. Exiting." ; exit 1;}