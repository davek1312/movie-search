# movie-search

## Overview
Create a simple login system using Laravel PHP Framework.

When a user correctly logs in to the system the user should see a form with a list of movie genres.

When the user chooses a genre, movies from that genre are retrieved and displayed on the page without reloading.

The user can then select a movie to see more details about the movie, again without reloading the page.

The user should also see an input box to search for movies by name. Please display the results in the same way as the genre results.

The Genres and Movies should be returned using the API from https://www.themoviedb.org/documentation/api. You will need to register to gain access to the API, see the FAQ for details https://www.themoviedb.org/faq/api

The purpose of this project is to determine your approach to the problem. Feel free to use any PHP and JavaScript tools you are comfortable with.


## Setup
```bash
bash docker/setup.sh
bash docker/movie-search-build.sh
```

## Laradock Configuration
Edit `laradock/.env`:
```text
PHP_VERSION=71
PHP_INTERPRETER=php-fpm
MYSQL_VERSION=8.0
MYSQL_DATABASE=movie_search
MYSQL_USER=movie_search
MYSQL_PASSWORD=
MYSQL_ROOT_PASSWORD=
```

## Laravel Configuration
Edit `.env`:
```text
DB_DATABASE=movie_search
DB_USERNAME=movie_search
DB_PASSWORD=

TMDB_API_KEY=
MOVIE_DATA_DRIVER=
```

## Docker Build
```bash
bash docker/docker-build.sh
```

## Deployment
```bash
bash deploy.sh
```