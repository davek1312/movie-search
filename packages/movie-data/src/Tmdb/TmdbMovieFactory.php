<?php

namespace MovieData\Movies\Tmdb;

use Illuminate\Pagination\LengthAwarePaginator;
use MovieData\Contracts\Movies\MovieFactory;
use Tmdb\Laravel\Facades\Tmdb;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbMovieFactory extends TmdbBaseFactory implements MovieFactory {

    /**
     * @param mixed $movieId
     * @param array $parameters
     *
     * @return TmdbMovie|null
     */
    public function getMovie($movieId, array $parameters = []) {
        $data = static::queryApi(function() use($movieId, $parameters) {
            return Tmdb::getMoviesApi()->getMovie($movieId, $parameters);
        });
        return static::createItem($data);
    }

    /**
     * @param mixed $genreId
     * @param array $parameters
     *
     * @return LengthAwarePaginator
     */
    public function getMoviesByGenre($genreId, array $parameters = []) {
        $data = static::queryApi(function() use($genreId, $parameters) {
            return Tmdb::getGenresApi()->getMovies($genreId, $parameters);
        });
        return static::createListPaginator($data);
    }

    /**
     * @param string $search
     * @param array $parameters
     *
     * @return LengthAwarePaginator
     */
    public function getMoviesBySearch($search, array $parameters = []) {
        $data =  static::queryApi(function() use($search, $parameters) {
            return Tmdb::getSearchApi()->searchMovies($search, $parameters);
        });
        return static::createListPaginator($data);
    }

    /**
     * @param array|null $data
     *
     * @return TmdbMovie|null
     */
    public static function createItem(array $data = null) {
        return !$data ? null : new TmdbMovie($data);
    }
}