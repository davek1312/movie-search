<?php

namespace MovieData\Movies\Tmdb;

use MovieData\Contracts\Tmdb\TmdbFactory;
use Tmdb\Exception\TmdbApiException;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class TmdbBaseFactory implements TmdbFactory {

    const FACTORY_NAME = 'tmdb';
    const MAX_PAGE = 1000;

    /**
     * @param callable $tmdbQeury
     *
     * @return array|null
     */
    protected static function queryApi(callable $tmdbQeury) {
        try {
            return $tmdbQeury();
        }
        catch(TmdbApiException $e) {
            return null;
        }
    }

    /**
     * @param array|null $data
     *
     * @return LengthAwarePaginator
     */
    protected static function createListPaginator(array $data = null) {
        $items = [];
        $total = 0;
        $perPage = 20;
        $page = 1;
        if(is_array($data) && isset($data['results'])) {
            $total = static::getPaginationTotal($data['total_results'], $perPage);
            $page = $data['page'];
            foreach($data['results'] as $item) {
                $items[] = static::createItem($item);
            }
        }
        $paginator = new LengthAwarePaginator($items, $total, $perPage, $page);
        return $paginator;
    }

    /**
     *  TODO: Remove this when API issue resolved. Hack to display at mostt 1000 pages due to issue highlighted here: https://www.themoviedb.org/talk/591edb669251414990031f4c
     *
     * @param integer $total
     * @param integer $perPage
     *
     * @return integer
     */
    private static function getPaginationTotal($total, $perPage) {
        $maxTotal = $perPage*static::MAX_PAGE;
        return $total > $maxTotal ? $maxTotal : $total;
    }

    /**
     * @param array|null $data
     *
     * @return array
     */
    protected static function createList(array $data = null) {
        $list = [];
        if(is_array($data)) {
            foreach($data as $item) {
                $list[] = static::createItem($item);
            }
        }
        return $list;
    }
}