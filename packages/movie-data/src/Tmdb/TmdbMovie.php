<?php

namespace MovieData\Movies\Tmdb;

use MovieData\Contracts\Movies\Movie;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbMovie extends TmdbBase implements Movie {

    /**
     * @return integer
     */
    public function getId() {
        return $this->getValueOrNull('id');
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->getValueOrNull('title');
    }

    /**
     * @return string
     */
    public function getReleaseDate() {
        return $this->getValueOrNull('release_date');
    }

    /**
     * @return string
     */
    public function getSynopsis() {
        return $this->getValueOrNull('overview');
    }
}