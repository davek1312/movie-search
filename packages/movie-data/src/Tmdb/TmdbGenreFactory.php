<?php

namespace MovieData\Movies\Tmdb;

use MovieData\Contracts\Genres\GenreFactory;
use Tmdb\Laravel\Facades\Tmdb;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbGenreFactory extends TmdbBaseFactory implements GenreFactory {

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function getGenres(array $parameters = []) {
        $data = $this->queryApi(function() use($parameters) {
            $genres = Tmdb::getGenresApi()->getMovieGenres($parameters);
            return isset($genres['genres']) ? $genres['genres'] : [];
        });
        return static::createList($data);
    }

    /**
     * @param array|null $data
     *
     * @return TmdbGenre|null
     */
    public static function createItem(array $data = null) {
        return !$data ? null : new TmdbGenre($data);
    }
}