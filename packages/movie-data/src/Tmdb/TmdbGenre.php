<?php

namespace MovieData\Movies\Tmdb;

use MovieData\Contracts\Genres\Genre;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbGenre extends TmdbBase implements Genre {

    /**
     * @return integer
     */
    public function getId() {
        return $this->getValueOrNull('id');
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->getValueOrNull('name');
    }
}