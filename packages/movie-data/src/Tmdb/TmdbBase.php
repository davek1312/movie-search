<?php

namespace MovieData\Movies\Tmdb;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class TmdbBase {

    /**
     * @var array
     */
    protected $data;

    public function __construct(array $data) {
        $this->data = $data;
    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    protected function getValueOrNull($key) {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data) {
        $this->data = $data;
    }
}