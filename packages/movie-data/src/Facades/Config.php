<?php

namespace MovieData\Movies\Facades;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class Config {

    const BASE_CONFIG_PATH = 'movie-data';

    public static function getDriver() {
        return static::getConfig('driver');
    }

    /**
     * @param string $path
     *
     * @return mixed
     */
    private static function getConfig($path) {
        return config(static::BASE_CONFIG_PATH.".{$path}");
    }
}