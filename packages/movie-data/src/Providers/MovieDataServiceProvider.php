<?php

namespace MovieData\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * ServiceProvider for the movie-data package
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class MovieDataServiceProvider extends ServiceProvider {

    const PACKAGE_NAME = 'movie-data';

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot() {
        $this->bootConfig();
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register() {
    }

    /**
     * Publish the package's public assets
     *
     * @return void
     */
    private function bootConfig() {
        $this->publishes([
            __DIR__.'/../../config/movie-data.php' => config_path(static::PACKAGE_NAME.'.php'),
        ], 'config');
    }
}
