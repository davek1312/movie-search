<?php

namespace MovieData\Contracts\Genres;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
interface GenreFactory {

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function getGenres(array $parameters = []);
}