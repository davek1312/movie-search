<?php

namespace MovieData\Contracts\Genres;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
interface Genre {

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();
}