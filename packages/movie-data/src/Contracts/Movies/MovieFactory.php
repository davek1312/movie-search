<?php

namespace MovieData\Contracts\Movies;

use Illuminate\Pagination\LengthAwarePaginator;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
interface MovieFactory {

    /**
     * @param mixed $movieId
     * @param array $parameters
     *
     * @return Movie
     */
    public function getMovie($movieId, array $parameters = []);

    /**
     * @param mixed $genreId
     * @param array $parameters
     *
     * @return LengthAwarePaginator
     */
    public function getMoviesByGenre($genreId, array $parameters = []);

    /**
     * @param string $search
     * @param array $parameters
     *
     * @return LengthAwarePaginator
     */
    public function getMoviesBySearch($search, array $parameters = []);
}