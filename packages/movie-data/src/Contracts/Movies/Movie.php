<?php

namespace MovieData\Contracts\Movies;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
interface Movie {

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getReleaseDate();

    /**
     * @return string
     */
    public function getSynopsis();
}