<?php

namespace MovieData\Contracts\Tmdb;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
interface TmdbFactory {

    /**
     * @param array|null $data
     *
     * @return mixed
     */
    public static function createItem(array $data = null);
}