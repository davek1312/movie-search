<?php

namespace MovieData\Movies\Exceptions;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class FactoryNotFoundException extends \Exception {

    /**
     * @var string
     */
    protected $message = 'Factory not found';
}