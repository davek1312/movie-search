<?php

namespace MovieData\Movies\Genre;

use MovieData\Movies\Common\BaseManager;
use MovieData\Movies\Genres\GenreFactoryResolver;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class GenreManager extends BaseManager {

    /**
     * @param string $driver
     */
    public function __construct($driver = null) {
        parent::__construct($driver);
        $this->factoryResolver = new GenreFactoryResolver();
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function getGenres(array $parameters = []) {
        return $this->getFactory()->getGenres($parameters);
    }
}