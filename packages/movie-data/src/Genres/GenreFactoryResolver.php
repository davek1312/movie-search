<?php

namespace MovieData\Movies\Genres;

use MovieData\Movies\Common\BaseFactoryResolver;
use MovieData\Movies\Tmdb\TmdbGenreFactory;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class GenreFactoryResolver extends BaseFactoryResolver {

    public function __construct() {
        $this->factories = [
            TmdbGenreFactory::FACTORY_NAME => new TmdbGenreFactory(),
        ];
    }
}