<?php

namespace MovieData\Movies\Movies;

use MovieData\Contracts\Movies\Movie;
use MovieData\Movies\Common\BaseManager;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class MovieManager extends BaseManager {

    /**
     * @param string $driver
     */
    public function __construct($driver = null) {
        parent::__construct($driver);
        $this->factoryResolver = new MovieFactoryResolver();
    }

    /**
     * @param mixed $movieId
     * @param array $parameters
     *
     * @return Movie
     */
    public function getMovie($movieId, array $parameters = []) {
        return $this->getFactory()->getMovie($movieId, $parameters);
    }

    /**
     * @param mixed $genreId
     * @param array $parameters
     *
     * @return LengthAwarePaginator
     */
    public function getMoviesByGenre($genreId, array $parameters = []) {
        return $this->getFactory()->getMoviesByGenre($genreId, $parameters);
    }

    /**
     * @param string $search
     * @param array $parameters
     *
     * @return LengthAwarePaginator
     */
    public function getMoviesBySearch($search, array $parameters = []) {
        return $this->getFactory()->getMoviesBySearch($search, $parameters);
    }
}