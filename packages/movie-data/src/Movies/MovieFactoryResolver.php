<?php

namespace MovieData\Movies\Movies;

use MovieData\Movies\Common\BaseFactoryResolver;
use MovieData\Movies\Tmdb\TmdbMovieFactory;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class MovieFactoryResolver extends BaseFactoryResolver {

    public function __construct() {
        $this->factories = [
            TmdbMovieFactory::FACTORY_NAME => new TmdbMovieFactory(),
        ];
    }
}