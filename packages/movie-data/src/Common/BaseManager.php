<?php

namespace MovieData\Movies\Common;

use Exception;
use MovieData\Movies\Facades\Config;

/**
 * Base manager
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class BaseManager {

    /**
     * @var BaseFactoryResolver
     */
    protected $factoryResolver;

    /**
     * @var null|string
     */
    private $driver;

    /**
     * @param string $driver
     */
    public function __construct($driver = null) {
        $this->driver = $driver;
    }

    /**
     * @return BaseFactoryResolver
     */
    public function getFactoryResolver() {
        return $this->factoryResolver;
    }

    /**
     * @param BaseFactoryResolver $factoryResolver
     */
    public function setFactoryResolver($factoryResolver) {
        $this->factoryResolver = $factoryResolver;
    }

    /**
     * @return mixed
     */
    public function getFactory() {
        $driver = $this->driver ? $this->driver : Config::getDriver();
        $this->factoryResolver->setDriver($driver);
        return $this->factoryResolver->resolveFactory();
    }

    /**
     * @return null|string
     */
    public function getDriver() {
        return $this->driver;
    }

    /**
     * @param null|string $driver
     */
    public function setDriver($driver) {
        $this->driver = $driver;
    }
}