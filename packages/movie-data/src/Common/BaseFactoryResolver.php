<?php

namespace MovieData\Movies\Common;

use MovieData\Movies\Exceptions\FactoryNotFoundException;

/**
 * Base factory resolver
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class BaseFactoryResolver {

    /**
     * @var array
     */
    protected $factories = [];

    /**
     * @var string
     */
    private $driver;

    /**
     * @throws FactoryNotFoundException
     *
     * @return mixed
     */
    public function resolveFactory()  {
        $factory = isset($this->factories[$this->driver]) ? $this->factories[$this->driver] : null;
        if(!$factory) {
            throw new FactoryNotFoundException();
        }
        return $factory;
    }

    /**
     * @return array
     */
    public function getFactories() {
        return $this->factories;
    }

    /**
     * @param array $factories
     */
    public function setFactories($factories) {
        $this->factories = $factories;
    }

    /**
     * @return string
     */
    public function getDriver() {
        return $this->driver;
    }

    /**
     * @param string $driver
     */
    public function setDriver($driver) {
        $this->driver = $driver;
    }
}