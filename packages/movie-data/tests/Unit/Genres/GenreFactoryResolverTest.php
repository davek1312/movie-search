<?php

namespace MovieData\Tests\Unit\Genres;

use MovieData\Movies\Genres\GenreFactoryResolver;
use MovieData\Movies\Tmdb\TmdbGenreFactory;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class GenreFactoryResolverTest extends TestCase {

    /**
     * @var GenreFactoryResolver
     */
    private $factoryResolver;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->factoryResolver = new GenreFactoryResolver();
    }

    /**
     * @return void
     */
    public function test__construct() {
        $this->assertEquals([
            TmdbGenreFactory::FACTORY_NAME => new TmdbGenreFactory(),
        ], $this->factoryResolver->getFactories());
    }
}
