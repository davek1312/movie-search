<?php

namespace MovieData\Tests\Unit\Genres;

use MovieData\Movies\Genre\GenreManager;
use MovieData\Movies\Genres\GenreFactoryResolver;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class GenreManagerTest extends TestCase {

    /**
     * @var GenreManager
     */
    private $manager;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->manager = new GenreManager();
    }

    /**
     * @return void
     */
    public function test__construct() {
        $this->assertEquals(new GenreFactoryResolver(), $this->manager->getFactoryResolver());
    }
}
