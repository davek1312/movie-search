<?php

namespace MovieData\Tests\Unit\Movies;

use MovieData\Movies\Movies\MovieFactoryResolver;
use MovieData\Movies\Tmdb\TmdbMovieFactory;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class MovieFactoryResolverTest extends TestCase {

    /**
     * @var MovieFactoryResolver
     */
    private $factoryResolver;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->factoryResolver = new MovieFactoryResolver();
    }

    /**
     * @return void
     */
    public function test__construct() {
        $this->assertEquals([
            TmdbMovieFactory::FACTORY_NAME => new TmdbMovieFactory(),
        ], $this->factoryResolver->getFactories());
    }
}
