<?php

namespace MovieData\Tests\Unit\Movies;

use MovieData\Movies\Movies\MovieFactoryResolver;
use MovieData\Movies\Movies\MovieManager;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class MovieManagerTest extends TestCase {

    /**
     * @var MovieManager
     */
    private $manager;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->manager = new MovieManager();
    }

    /**
     * @return void
     */
    public function test__construct() {
        $this->assertEquals(new MovieFactoryResolver(), $this->manager->getFactoryResolver());
    }
}
