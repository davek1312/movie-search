<?php

namespace MovieData\Tests\Unit\Tmdb;

use MovieData\Movies\Tmdb\TmdbMovie;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbMovieTest extends TestCase {

    /**
     * @var array
     */
    private $data;
    /**
     * @var TmdbMovie
     */
    private $movie;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->data = [
            'id' => 1,
            'title' => 'title',
            'release_date' => 'date',
            'overview' => 'overview',
        ];
        $this->movie = new TmdbMovie($this->data);
    }

    /**
     * @return void
     */
    public function testGetId() {
        $this->assertEquals($this->data['id'], $this->movie->getId());
    }

    /**
     * @return void
     */
    public function testGetTitle() {
        $this->assertEquals($this->data['title'], $this->movie->getTitle());
    }

    /**
     * @return void
     */
    public function testGetReleaseDate() {
        $this->assertEquals($this->data['release_date'], $this->movie->getReleaseDate());
    }

    /**
     * @return void
     */
    public function testGetSynopsis() {
        $this->assertEquals($this->data['overview'], $this->movie->getSynopsis());
    }
}
