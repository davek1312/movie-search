<?php

namespace MovieData\Tests\Unit\Tmdb;

use MovieData\Movies\Tmdb\TmdbBase;
use MovieData\Tests\TestCase;
use \Mockery;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbBaseTest extends TestCase {

    /**
     * @var array
     */
    private $data;
    /**
     * @var TmdbBase
     */
    private $tmdb;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->data = ['test'];
        $this->tmdb = Mockery::mock(TmdbBase::class, [$this->data])->makePartial();
    }

    /**
     * @return void
     */
    public function test__construct() {
        $this->assertSame($this->data, $this->tmdb->getData());
    }
}
