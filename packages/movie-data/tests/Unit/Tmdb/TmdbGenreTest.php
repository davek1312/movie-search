<?php

namespace MovieData\Tests\Unit\Tmdb;

use MovieData\Movies\Tmdb\TmdbGenre;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbGenreTest extends TestCase {

    /**
     * @var array
     */
    private $data;
    /**
     * @var TmdbGenre
     */
    private $genre;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->data = [
            'id' => 1,
            'name' => 'name'
        ];
        $this->genre = new TmdbGenre($this->data);
    }

    /**
     * @return void
     */
    public function testGetId() {
        $this->assertEquals($this->data['id'], $this->genre->getId());
    }

    /**
     * @return void
     */
    public function testGetName() {
        $this->assertEquals($this->data['name'], $this->genre->getName());
    }
}
