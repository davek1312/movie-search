<?php

namespace MovieData\Tests\Unit\Tmdb;

use MovieData\Movies\Tmdb\TmdbMovie;
use MovieData\Movies\Tmdb\TmdbMovieFactory;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbMovieFactoryTest extends TestCase {

    /**
     * @return void
     */
    public function testCreateItem() {
        $this->assertNull(TmdbMovieFactory::createItem(null));

        $data = ['test'];
        $item = TmdbMovieFactory::createItem($data);
        $this->assertInstanceOf(TmdbMovie::class, $item);
        $this->assertSame($data, $item->getData());
    }
}
