<?php

namespace MovieData\Tests\Unit\Tmdb;

use MovieData\Movies\Tmdb\TmdbGenre;
use MovieData\Movies\Tmdb\TmdbGenreFactory;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbGenreFactoryTest extends TestCase {

    /**
     * @return void
     */
    public function testCreateItem() {
        $this->assertNull(TmdbGenreFactory::createItem(null));

        $data = ['test'];
        $item = TmdbGenreFactory::createItem($data);
        $this->assertInstanceOf(TmdbGenre::class, $item);
        $this->assertSame($data, $item->getData());
    }
}
