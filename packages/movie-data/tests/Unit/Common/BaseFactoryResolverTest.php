<?php

namespace MovieData\Tests\Unit\Common;

use MovieData\Movies\Common\BaseFactoryResolver;
use MovieData\Movies\Exceptions\FactoryNotFoundException;
use MovieData\Tests\TestCase;
use \Mockery;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class BaseFactoryResolverTest extends TestCase {

    /**
     * @var BaseFactoryResolver
     */
    private $invalidFactoryResolver;
    /**
     * @var BaseFactoryResolver
     */
    private $validFactoryResolver;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->invalidFactoryResolver = Mockery::mock(BaseFactoryResolver::class)->makePartial();
        $this->validFactoryResolver = Mockery::mock(BaseFactoryResolver::class)->makePartial();
        $this->validFactoryResolver->setDriver('test');
        $this->validFactoryResolver->setFactories([
            'test' => 'test'
        ]);
        $this->assertEquals('test', $this->validFactoryResolver->resolveFactory());
    }

    /**
     * @return void
     */
    public function testResolveFactory() {
        $this->assertEquals('test', $this->validFactoryResolver->resolveFactory());

        $this->expectException(FactoryNotFoundException::class);
        $this->invalidFactoryResolver->resolveFactory();
    }
}
