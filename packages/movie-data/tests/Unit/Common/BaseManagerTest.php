<?php

namespace MovieData\Tests\Unit\Common;

use MovieData\Movies\Common\BaseFactoryResolver;
use MovieData\Movies\Common\BaseManager;
use MovieData\Movies\Facades\Config;
use MovieData\Tests\TestCase;
use \Mockery;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class BaseManagerTest extends TestCase {

    /**
     * @var string
     */
    private $driver = 'test';
    /**
     * @var BaseManager
     */
    private $manager;
    /**
     * @var BaseFactoryResolver
     */
    private $factoryResolver;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->manager = Mockery::mock(BaseManager::class, [$this->driver])->makePartial();
        $this->factoryResolver =  Mockery::mock(BaseFactoryResolver::class)->makePartial();
        $this->factoryResolver->setFactories([
            $this->driver => $this->driver,
            Config::getDriver() => 'tmdb'
        ]);
        $this->manager->setFactoryResolver($this->factoryResolver);
    }

    /**
     * @return void
     */
    public function test__construct() {
        $this->assertEquals($this->driver, $this->manager->getDriver());

        $manager = Mockery::mock(BaseManager::class)->makePartial();
        $this->assertNull($manager->getDriver());
    }

    /**
     * @return void
     */
    public function testGetFactory() {
        $this->assertEquals($this->driver, $this->manager->getFactory());
        $this->manager->setDriver(null);
        $this->assertEquals('tmdb', $this->manager->getFactory());
    }
}
