<?php

namespace MovieData\Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Base test class. Rolls back db transactions.
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class TestCase extends \Tests\TestCase {

    use DatabaseTransactions;
}
