<?php

namespace MovieData\Tests\Feature\Facades;

use MovieData\Tests\TestCase;
use MovieData\Movies\Facades\Config;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class ConfigTest extends TestCase {

    /**
     * @return void
     */
    public function testGetDriver() {
        $this->assertEquals('tmdb', Config::getDriver());
    }
}
