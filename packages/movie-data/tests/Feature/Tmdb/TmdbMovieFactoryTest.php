<?php

namespace MovieData\Tests\Feature\Tmdb;

use MovieData\Movies\Tmdb\TmdbMovie;
use MovieData\Movies\Tmdb\TmdbMovieFactory;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbMovieFactoryTest extends TestCase {

    /**
     * @var TmdbMovieFactory
     */
    private $factory;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->factory = new TmdbMovieFactory();
    }

    /**
     * @return void
     */
    public function testGetMovie() {
        $id = -1;
        $this->assertNull($this->factory->getMovie($id));

        $id = 283995;
        $movie = $this->factory->getMovie($id);
        $this->assertNotNull($movie);
        $this->assertEquals($id, $movie->getId());
    }

    /**
     * @return void
     */
    public function testGetMoviesByGenre() {
        $id = -1;
        $movies = $this->factory->getMoviesByGenre($id);
        $this->assertEquals(0, $movies->total());

        $id = 28;
        $movies = $this->factory->getMoviesByGenre($id);
        $this->assertGreaterThan(0, $movies->total());
        foreach($movies as $movie) {
            $this->assertInstanceOf(TmdbMovie::class, $movie);
        }
    }

    /**
     * @return void
     */
    public function testGetMoviesBySearch() {
        $search = 'test';
        $movies = $this->factory->getMoviesBySearch($search);
        $this->assertGreaterThan(0, $movies->total());
        foreach($movies as $movie) {
            $this->assertInstanceOf(TmdbMovie::class, $movie);
        }
    }
}
