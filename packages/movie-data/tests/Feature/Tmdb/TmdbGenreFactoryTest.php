<?php

namespace MovieData\Tests\Feature\Tmdb;

use MovieData\Movies\Tmdb\TmdbGenre;
use MovieData\Movies\Tmdb\TmdbGenreFactory;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class TmdbGenreFactoryTest extends TestCase {

    /**
     * @var TmdbGenreFactory
     */
    private $factory;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->factory = new TmdbGenreFactory();
    }

    /**
     * @return void
     */
    public function testGetGenres() {
        $genres = $this->factory->getGenres();
        foreach($genres as $genre) {
            $this->assertInstanceOf(TmdbGenre::class, $genre);
        }
    }
}
