<?php

namespace MovieData\Tests\Feature\Movies;

use MovieData\Movies\Movies\MovieManager;
use MovieData\Movies\Tmdb\TmdbMovie;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class MovieManagerTest extends TestCase {

    /**
     * @var MovieManager
     */
    private $manager;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->manager = new MovieManager();
    }

    /**
     * @return void
     */
    public function testGetMovie() {
        $id = -1;
        $this->assertNull($this->manager->getMovie($id));

        $id = 283995;
        $movie = $this->manager->getMovie($id);
        $this->assertNotNull($movie);
        $this->assertInstanceOf(TmdbMovie::class, $movie);
        $this->assertEquals($id, $movie->getId());
    }

    /**
     * @return void
     */
    public function testGetMoviesByGenre() {
        $id = 28;
        $movies = $this->manager->getMoviesByGenre($id);
        $this->assertGreaterThan(0, $movies->total());
        foreach($movies as $movie) {
            $this->assertInstanceOf(TmdbMovie::class, $movie);
        }
    }

    /**
     * @return void
     */
    public function testGetMoviesBySearch() {
        $search = 'test';
        $movies = $this->manager->getMoviesBySearch($search);
        $this->assertGreaterThan(0, $movies->total());
        foreach($movies as $movie) {
            $this->assertInstanceOf(TmdbMovie::class, $movie);
        }
    }
}