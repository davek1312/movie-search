<?php

namespace MovieData\Tests\Feature\Genres;

use MovieData\Movies\Genre\GenreManager;
use MovieData\Movies\Tmdb\TmdbGenre;
use MovieData\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class GenreManagerTest extends TestCase {

    /**
     * @var GenreManager
     */
    private $manager;

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->manager = new GenreManager();
    }

    /**
     * @return void
     */
    public function testGetGenres() {
        $genres = $this->manager->getGenres();
        foreach($genres as $genre) {
            $this->assertInstanceOf(TmdbGenre::class, $genre);
        }
    }
}
