var MovieSearch = function(movieResultsId, movieDetailsContentId, movieDetailsModalId) {

    var moviesSpan = $('#'+movieResultsId);
    var movieDetailsContentSpan = $('#'+movieDetailsContentId);
    var movieDetailsModal = $('#'+movieDetailsModalId);

    var queryServer = function(url, callback, data) {
        $.ajax({
            url: url,
            data:data,
            success: function(response) {
                callback(response);
        }});
    };
    
    var checkIsLoggedIn = function() {
        queryServer('/isloggedin', reloadIfNotLoggedIn);
    };

    var reloadIfNotLoggedIn = function(response) {
        response = JSON.parse(response);
        if(!response.logged_in) {
            location.reload();
        }
    };

    this.searchMoviesByGenre = function(genreId) {
        searchMoviesByAction('genre/'+genreId);
    };

    this.searchMoviesByQuery = function(query) {
        searchMoviesByAction('query/'+query);
    };

    this.searchMoviesUrl = function(url) {
        searchMovies(url);
    };

    var searchMovies = function(url, data) {
        checkIsLoggedIn();
        queryServer(url, displayMovies, data);
    };

    var searchMoviesByAction = function(action) {
        searchMovies('movies/'+action, {page: 1});
    };

    var displayMovies = function(response) {
        moviesSpan.html(response);
    };

    this.searchMovie = function(url) {
        checkIsLoggedIn();
        queryServer(url, displayMovieDetails);
    };

    var displayMovieDetails = function(response) {
        movieDetailsContentSpan.html(response);
        movieDetailsModal.modal();
    }
};