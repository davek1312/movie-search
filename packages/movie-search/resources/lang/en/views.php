<?php

return [

    'login' => 'Login',
    'logout' => 'Logout',
    'email' => 'Email',
    'password' => 'Password',
    'submit' => 'Submit',
    'movies' => 'Movies',
    'movie' => 'Movie',
    'genres' => 'Genres',
    'genre' => 'Genre',
    'select' => 'Select',
    'search' => 'Search',
    'name' => 'Name',
    'search-info' => 'Search movies by genre OR name',
    'title' => 'Title',
    'release-date' => 'Release Date',
    'no-results' => 'No results found. Please try different search parameters',
    'begin-search' => 'Use the above inputs to find movies',
    'paginated-movies' => 'Displaying :first - :last out of :total movies',
    'details' => 'Details',
    'overview' => 'Overview',
    'vote_average' => 'Vote Average',
    'vote_count' => 'Vote Count',

];