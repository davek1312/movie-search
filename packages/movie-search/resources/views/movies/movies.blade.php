@if($movies->count())

<div id="movieLinks">

    <table class="table table-striped">

        <thead>

            <tr>
                <th>@lang('movie-search::views.title')</th>
                <th>@lang('movie-search::views.release-date')</th>
            </tr>

        </thead>

        <tbody>

        @foreach($movies as $movie)

            <tr>
                <td><a href="{{ route('movies.movie', ['movieId' => $movie->getId()]) }}">{{ $movie->getTitle() }}</a></td>
                <td>{{ $movie->getReleaseDate() }}</td>
            </tr>

        @endforeach

        </tbody>

    </table>

</div>

<div class="text-center" id="paginatedMovieLinks">

    <p class="text-info">@lang('movie-search::views.paginated-movies', [
        'first' => $movies->firstItem(), 'last' => $movies->lastItem(), 'total' => $movies->total(),
    ])</p>

    {{ $movies->links() }}

</div>


@else

<div class="alert alert-info">

    @lang('movie-search::views.no-results')

</div>

@endif