@extends('movie-search::templates.master')

@section('title', trans('movie-search::views.movies'))

@section('master.content')

    <div class="panel panel-default">

        <div class="panel-heading">@lang('movie-search::views.movie') @lang('movie-search::views.search')</div>

        <div class="panel-body">

            <p class="text-info">@lang('movie-search::views.search-info')</p>

            <div class="row">

                <div class="col-md-6">

                    <form role="form" data-toggle="validator" id="searchMoviesByGenre">

                        <label for="genre" class="control-label">@lang('movie-search::views.genre')</label>

                        <div class="input-group has-feedback">

                            <select class="form-control" name="genre" id="genreValue" required>

                                <option value="">@lang('movie-search::views.select') @lang('movie-search::views.genre')..</option>
                                @if(isset($genres))

                                    @foreach($genres as $genre)
                                        <option value="{{ $genre->getId() }}">{{ $genre->getName() }}</option>
                                    @endforeach

                                @endif

                            </select>

                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">@lang('movie-search::views.search')</button>
                            </span>

                        </div>

                        <div class="help-block with-errors"></div>

                    </form>

                </div>

                <div class="col-md-6">

                    <form role="form" data-toggle="validator" id="searchMoviesByQuery">

                        <label for="query" class="control-label">@lang('movie-search::views.name')</label>

                        <div class="input-group has-feedback">

                            <input type="text" class="form-control" name="query" id="queryValue" required>

                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">@lang('movie-search::views.search')</button>
                            </span>

                        </div>

                        <div class="help-block with-errors"></div>

                    </form>

                </div>

            </div>

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-heading">@lang('movie-search::views.movies')</div>

        <div class="panel-body">

            <span id="movieResults">

                <div class="alert alert-info">

                    @lang('movie-search::views.begin-search')

                </div>

            </span>

        </div>

    </div>

    <div class="modal fade" id="movieDetailsModal" role="dialog">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">@lang('movie-search::views.movie') @lang('movie-search::views.details')</h4>
                </div>

                <div class="modal-body">

                    <span id="movieDetailsContent"></span>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>

    </div>

    <script>

        $(document).ready(function () {
            var movieSearch = new MovieSearch('movieResults', 'movieDetailsContent', 'movieDetailsModal');
            var searchMoviesByGenreForm = $('#searchMoviesByGenre');
            var searchMoviesByQueryForm = $('#searchMoviesByQuery');
            var genreValueInput = $('#genreValue');
            var queryValueInput = $('#queryValue');

            searchMoviesByGenreForm.submit(function(e) {
                e.preventDefault();
                movieSearch.searchMoviesByGenre(genreValueInput.val());
            });

            searchMoviesByQueryForm.submit(function(e) {
                e.preventDefault();
                movieSearch.searchMoviesByQuery(queryValueInput.val());
            });

            $(document).on('click', '#paginatedMovieLinks a', function(e) {
                e.preventDefault();
                movieSearch.searchMoviesUrl($(this).attr('href'));
            });

            $(document).on('click', '#movieLinks a', function(e) {
                e.preventDefault();
                movieSearch.searchMovie($(this).attr('href'));
            });
        });

    </script>

@endsection