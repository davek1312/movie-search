@if(isset($movie))

    <p>
        <b>@lang('movie-search::views.title'):</b> {{ $movie->getTitle() }}
    </p>

    <p>
        <b>@lang('movie-search::views.release-date'):</b> {{ $movie->getReleaseDate() }}
    </p>

    <p>
        <b>@lang('movie-search::views.overview'):</b> {{ $movie->getSynopsis() }}
    </p>

@endif