@extends('movie-search::templates.master')

@section('title', trans('movie-search::views.login'))

@section('master.content')

    <form action="{{ route('dologin') }}" method="post" role="form" data-toggle="validator">

        {{ csrf_field() }}

        <div class="panel panel-default">

            <div class="panel-heading">@lang('movie-search::views.login')</div>

            <div class="panel-body">

                <div class="form-group has-feedback">
                    <label for="email" class="control-label">@lang('movie-search::views.email')</label>
                    <input type="text" name="email" class="form-control" id="email" placeholder="@lang('movie-search::views.email')" required>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group has-feedback">
                    <label for="password" class="control-label">@lang('movie-search::views.password')</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="@lang('movie-search::views.password')" required>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">@lang('movie-search::views.submit')</button>
                </div>

            </div>

        </div>

    </form>

@endsection