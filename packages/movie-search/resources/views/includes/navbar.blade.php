<nav class="navbar navbar-default">

    <div class="container">

        <div class="container-fluid">

            <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>

                </button>

                <a class="navbar-brand" href="{{ route('home.index') }}">{{ Config::get('app.name') }}</a>

            </div><!-- /.navbar-header -->

            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right">


                    @if(Auth::guest())

                        <li>
                            <a href="{{ route('login') }}">@lang('movie-search::views.login')</a>
                        </li>

                    @else

                        <li>
                            <a href="{{ route('dologout') }}">@lang('movie-search::views.logout')</a>
                        </li>

                    @endif

                </ul>

            </div><!--/.nav-collapse -->

        </div><!--/.container-fluid -->

    </div><!-- /.container -->

</nav>