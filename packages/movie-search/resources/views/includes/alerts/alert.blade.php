@if(session("alert-$alertLevel"))

    <div class="alert alert-{{ $alertLevel }}">

        <strong>{{ session("alert-$alertLevel") }}</strong>

    </div>

@endif