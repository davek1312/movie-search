<?php

Route::group(['middleware' => 'web'], function () {

    Route::group(['namespace' => '\MovieSearch\Http\Controllers'], function () {

        Route::get('/', 'HomeController@showIndex')->name('home.index');

        Route::get('/isloggedin', 'AuthController@isLoggedIn')->name('isloggedin');

        Route::group(['middleware' => 'guest'], function () {

            Route::get('/login', 'AuthController@showLogin')->name('login');

            Route::post('/dologin', 'AuthController@doLogin')->name('dologin');
        });

        Route::group(['middleware' => 'auth'], function () {

            Route::get('/dologout', 'AuthController@doLogout')->name('dologout');

            Route::group(['prefix' => 'movies'], function () {

                Route::get('/', 'MoviesController@showIndex')->name('movies.index');

                Route::get('/genre/{genreid}', 'MoviesController@genre')->name('movies.genre');

                Route::get('/query/{query}', 'MoviesController@query')->name('movies.query');

                Route::get('/movie/{movieId}', 'MoviesController@movie')->name('movies.movie');
            });
        });
    });
});