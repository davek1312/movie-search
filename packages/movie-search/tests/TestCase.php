<?php

namespace MovieSearch\Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use Auth;

/**
 * Base test class. Rolls back db transactions.
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class TestCase extends \Tests\TestCase {

    use DatabaseTransactions;

    /**
     * Create a user session
     *
     * @return void
     */
    protected function logUserIn() {
        $user = factory(User::class)->create();
        Auth::login($user);
    }
}
