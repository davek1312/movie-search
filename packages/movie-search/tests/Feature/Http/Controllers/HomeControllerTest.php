<?php

namespace MovieSearch\Tests\Feature\Http\Controllers;

use MovieSearch\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class HomeControllerTest extends TestCase {

    /**
     * @return void
     */
    public function testShowIndex() {
        $response = $this->get('/');
        $response->assertRedirect('/login');

        $this->logUserIn();
        $response = $this->get('/');
        $response->assertRedirect('/movies');
    }
}
