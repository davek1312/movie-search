<?php

namespace MovieSearch\Tests\Feature\Http\Controllers;

use MovieSearch\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class MoviesControllerTest extends TestCase {

    /**
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->logUserIn();
    }

    /**
     * @return void
     */
    public function testShowIndex() {
        $response = $this->get('/movies');
        $response->assertSeeText('Movie Search');
        $response->assertViewHas('genres');
    }

    /**
     * @return void
     */
    public function testGenre() {
        $response = $this->get('/movies/genre/28');
        $response->assertViewHas('movies');
    }

    /**
     * @return void
     */
    public function testQuery() {
        $response = $this->get('/movies/query/test');
        $response->assertViewHas('movies');
    }

    public function testMovie() {
        $response = $this->get('/movies/movie/283995');
        $response->assertViewHas('movie');
    }
}
