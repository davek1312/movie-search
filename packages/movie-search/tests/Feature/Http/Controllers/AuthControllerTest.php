<?php

namespace MovieSearch\Tests\Feature\Http\Controllers;

use App\User;
use MovieSearch\Tests\TestCase;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
class AuthControllerTest extends TestCase {

    /**
     * @return void
     */
    public function testShowLogin() {
        $response = $this->get('/login');
        $response->assertSee('/dologin');
    }

    /**
     * @return void
     */
    public function testDoLogin() {
        $this->assertDoLoginValidationFailed();
        $this->assertDoLoginInvalidCredentials();
        $this->assertDoLoginValidCredentials();
    }

    /**
     * @return void
     */
    private function assertDoLoginValidationFailed() {
        $response = $this->post('/dologin');
        $response->assertRedirect('/');
        $response->assertSessionHasErrors();
    }

    /**
     * @return void
     */
    private function assertDoLoginInvalidCredentials() {
        $response = $this->post('/dologin', [
            'email' => str_random(5),
            'password' => str_random(5),
        ]);
        $response->assertRedirect('/login');
        $response->assertSessionHas('alert-warning');
    }

    /**
     * @return void
     */
    private function assertDoLoginValidCredentials() {
        $password = str_random(6);
        $user = factory(User::class)->create([
            'password' => bcrypt($password),
        ]);
        $response = $this->post('/dologin', [
            'email' => $user->email,
            'password' => $password,
        ]);
        $response->assertRedirect('/movies');
    }

    /**
     * @return void
     */
    public function testDoLogout() {
        $response = $this->get('/dologout');
        $response->assertRedirect('/login');
    }

    public function testIsLoggedIn() {
        $this->assertEquals([
            'logged_in' => false
        ], $this->get('/isloggedin')->decodeResponseJson());

        $this->logUserIn();
        $this->assertEquals([
            'logged_in' => true
        ], $this->get('/isloggedin')->decodeResponseJson());
    }
}
