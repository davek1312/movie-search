<?php

namespace MovieSearch\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;

/**
 * Handles the index page
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class HomeController extends Controller {

    /**
     * Show the index page
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function showIndex() {
        if(Auth::guest()) {
            return redirect()->route('login');
        }
        return redirect()->route('movies.index');
    }
}