<?php

namespace MovieSearch\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

/**
 * Handles authentication routes
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class AuthController extends Controller {

    /**
     * Show the login page
     *
     * @return \Illuminate\View\View
     */
    public function showLogin() {
        return view('movie-search::auth.login');
    }

    /**
     * Performs the login action
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doLogin(Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        if($this->attemptLogin($request)) {
            return redirect()->route('movies.index');
        }
        return redirect()->route('login')->with('alert-warning', trans('auth.failed'));
    }

    /**
     * @param Request $request
     *
     * @return boolean
     */
    private function attemptLogin(Request $request) {
        return Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
    }

    /**
     * Performs the logout action
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doLogout() {
        Auth::logout();
        return redirect()->route('home.index');
    }

    /**
     * @return string
     */
    public function isLoggedIn() {
        return json_encode([
            'logged_in' => Auth::check(),
        ]);
    }
}