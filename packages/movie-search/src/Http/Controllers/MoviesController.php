<?php

namespace MovieSearch\Http\Controllers;

use App\Http\Controllers\Controller;
use MovieData\Movies\Genre\GenreManager;
use MovieData\Movies\Movies\MovieManager;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

/**
 * Handles the movies pages
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class MoviesController extends Controller {

    /**
     * Show the movie index page
     *
     * @return \Illuminate\View\View
     */
    public function showIndex() {
        return view('movie-search::movies.index', [
            'genres' => $this->getGenres(),
        ]);
    }

    /**
     * @return array
     */
    private function getGenres() {
        $manager = new GenreManager();
        return $manager->getGenres();
    }

    /**
     * Show movies by genre
     *
     * @param integer $genreId
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function genre($genreId, Request $request) {
        $manager = new MovieManager();
        return $this->getMoviesView($manager->getMoviesByGenre($genreId, $this->getMovieParameters($request)));
    }

    /**
     * Show movies by genre
     *
     * @param string $query
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function query($query, Request $request) {
        $manager = new MovieManager();
        return $this->getMoviesView($manager->getMoviesBySearch($query, $this->getMovieParameters($request)));
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function getMovieParameters(Request $request) {
        return $request->only([
            'page',
        ]);
    }

    /**
     * Get the movies view
     *
     * @param LengthAwarePaginator $movies
     *
     * @return \Illuminate\View\View
     */
    private function getMoviesView($movies) {
        $movies->setPath(url()->current());
        return view('movie-search::movies.movies', [
            'movies' => $movies,
        ]);
    }

    /**
     * Show movie by id
     *
     * @param integer $movieId
     *
     * @return \Illuminate\View\View
     */
    public function movie($movieId) {
        $manager = new MovieManager();
        return view('movie-search::movies.movie', [
            'movie' => $manager->getMovie($movieId),
        ]);
    }
}