<?php

namespace MovieSearch\Providers;

use Illuminate\Support\ServiceProvider;
use MovieData\Providers\MovieDataServiceProvider;
use Schema;
use Tmdb\Laravel\TmdbServiceProvider;
use Tmdb\Laravel\TmdbServiceProviderLaravel5;

/**
 * ServiceProvider for the movie-search package
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class MovieSearchServiceProvider extends ServiceProvider {

    const PACKAGE_NAME = 'movie-search';

    /**
     * @var array
     */
    private $serviceProviders = [
        TmdbServiceProvider::class,
        TmdbServiceProviderLaravel5::class,
        MovieDataServiceProvider::class,
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot() {
        $this->bootSchemaDefaultStringLength();
        $this->bootViews();
        $this->bootRoutes();
        $this->bootTranslation();
        $this->bootPublicAssets();
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register() {
        $this->registerServiceProviders();
    }

    /**
     * Fixes issue outlined here: https://laracasts.com/discuss/channels/laravel/laravel-54-failing-on-php-artisan-migrate-after-php-artisan-makeauth
     *
     * @return void
     */
    private function bootSchemaDefaultStringLength() {
        Schema::defaultStringLength(191);
    }

    /**
     * Load the package views
     *
     * @return void
     */
    private function bootViews() {
        $this->loadViewsFrom(__DIR__.'/../../resources/views', static::PACKAGE_NAME);
    }

    /**
     * Load the package routes
     *
     * @return void
     */
    private function bootRoutes() {
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    }

    /**
     * Load the package translations
     *
     * @return void
     */
    private function bootTranslation() {
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', static::PACKAGE_NAME);
    }

    /**
     * Register other service providers used by the package
     *
     * @return void
     */
    private function registerServiceProviders() {
        foreach($this->serviceProviders as $serviceProvider) {
            $this->app->register($serviceProvider);
        }
    }

    /**
     * Publish the package's public assets
     *
     * @return void
     */
    private function bootPublicAssets() {
        $this->publishes([
            __DIR__.'/../../resources/assets' => public_path('movie-search'),
        ], 'public');
    }
}
